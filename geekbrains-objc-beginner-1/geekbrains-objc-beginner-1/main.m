//
//  main.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 26.01.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


#define MY_AGE 25
#define DAYS_IN_YEAR 365.25


int main(int argc, char * argv[]) {
    
    NSInteger myAgeInTenYears = MY_AGE + 10;
    CGFloat daysPassed = myAgeInTenYears * DAYS_IN_YEAR;
    NSLog(@"Через 10 лет мне будет %ld лет, c момента моего рождения пройдет %.2f дней", (long)myAgeInTenYears, daysPassed);
    
    NSUInteger firstVar = 26;
    NSUInteger secondVar = 5;
    NSInteger result = firstVar / secondVar;
    NSInteger redaminder = firstVar % secondVar;
    NSLog(@"При делении %lu на %lu результат равен %ld, остаток равен %ld", (unsigned long)firstVar, (unsigned long)secondVar, (long)result, (long)redaminder);
    NSLog(@"Результат деления %lu на %lu равен %ld %ld/%ld", (unsigned long)firstVar, (unsigned long)secondVar, (long)result, (long)redaminder, (unsigned long)secondVar);
    
    NSString *firstString = @"I can";
    NSString *secondString = @"code";
    NSLog(@"%@ %@!", firstString, secondString);
    
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
