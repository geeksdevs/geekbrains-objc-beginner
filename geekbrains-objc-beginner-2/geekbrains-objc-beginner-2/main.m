//
//  main.m
//  geekbrains-objc-beginner-2
//
//  Created by Mikhail on 02.02.17.
//  Copyright © 2017 Appvilion Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


typedef NS_ENUM(NSInteger, CalculatonType) {
    CalculatonTypemMult,  // Умножение
    CalculatonTypeDiv, // Деление
    CalculatonTypeAdd, // Сложение
    CalculatonTypeSubt // Вычитание
};


NSInteger mathOperation(CalculatonType type, NSInteger firstInt, NSInteger secondInt) {
    switch (type) {
        case CalculatonTypemMult:
            return firstInt * secondInt;
        case CalculatonTypeDiv:
            return firstInt / secondInt;
        case CalculatonTypeAdd:
            return firstInt + secondInt;
        case CalculatonTypeSubt:
            return firstInt - secondInt;
    }
    return 0;
}

NSString *mathSymbol(CalculatonType type) {
    switch (type) {
        case CalculatonTypemMult:
            return @"*";
        case CalculatonTypeDiv:
            return @"/";
        case CalculatonTypeAdd:
            return @"+";
        case CalculatonTypeSubt:
            return @"-";
    }
    return @"";
}

NSUInteger quadMultiplier(NSUInteger unsNumber) {
    return unsNumber * 4;
}

NSUInteger factorial(NSUInteger n) {
    NSUInteger fact = 1;
    for (int i = 2; i <= n; ++i) {
        fact *= i;
    }
    return fact;
}

int main(int argc, char * argv[]) {
    
    NSInteger firstInt = 20;
    NSInteger secondInt = 5;
    
    NSLog(@"Результат %ld %@ %ld равен %ld",(long)firstInt, mathSymbol(CalculatonTypemMult), (long)secondInt, (long)mathOperation(CalculatonTypemMult, firstInt, secondInt));
    NSLog(@"Результат %ld %@ %ld равен %ld",(long)firstInt, mathSymbol(CalculatonTypeDiv), (long)secondInt, (long)mathOperation(CalculatonTypeDiv, firstInt, secondInt));
    NSLog(@"Результат %ld %@ %ld равен %ld",(long)firstInt, mathSymbol(CalculatonTypeAdd), (long)secondInt, (long)mathOperation(CalculatonTypeAdd, firstInt, secondInt));
    NSLog(@"Результат %ld %@ %ld равен %ld",(long)firstInt, mathSymbol(CalculatonTypeSubt), (long)secondInt, (long)mathOperation(CalculatonTypeSubt, firstInt, secondInt));
    
    NSLog(@"quadMultiplier %ld = %lu", (long)firstInt, (unsigned long)quadMultiplier((NSUInteger)firstInt));
    
    int count = 0;
    for (int i=1; i <= 20*2; i++) {
        count++;
        i++;
        
        NSUInteger result = quadMultiplier((NSUInteger)i);
        if (result == 16) {
            break;
            
        } else if (result % 3 != 0) {
            NSLog(@"Число %d не кратно 3", i);

        } else {
            NSLog(@"Число %d кратно 3", i);
            continue;
        }

        NSLog(@"Осталось %d итераций", 20 - count);
    }
    
    NSLog(@"Факториал firstInt %lu", (unsigned long)factorial((NSUInteger)firstInt));
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}


