//
//  Orange.h
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Orange : NSObject

@property (strong, nonatomic) NSString *color;
@property (strong, nonatomic) NSString *taste;
@property (assign, nonatomic) NSInteger radius;

- (double)orangeVolume;

@end
