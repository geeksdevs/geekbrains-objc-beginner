//
//  Orange.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import "Orange.h"

@implementation Orange

- (double)orangeVolume {
     return (4.0/3.0)*M_PI * pow(self.radius, 3);
}

@end
