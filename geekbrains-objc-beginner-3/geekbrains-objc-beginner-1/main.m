//
//  main.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 26.01.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Orange.h"

//3. В файле main.m создайте структуру Car со следующими элементами:
//— NSString *name
//— unsigned productionYear
//— unsigned horsePower

struct {
    NSString *name;
    NSUInteger productionYear;
    NSUInteger horsePower;
} typedef Car;


//4. В теле функции main создайте экземпляр структуры Car (другими словами, объявите
//                                                         переменную типа Car) с именем, например, honda.

int main(int argc, char * argv[]) {
   
    //    Car car;
    
//    5. Проинициализируйте каждый элемент структуры В ОТДЕЛЬНОСТИ любыми
//    значениями (желательно правдоподобными, например: @«Honda», 2012, 132).
    

//    car.name = @"Honda";
//    car.productionYear = 2012;
//    car.horsePower = 132;
    
    
//    6. Закомментируйте код из п. 5 и проинициализируйте все элементы одной строкой.
//    Потом закомментируйте всё, что написали в main, и реализуйте объявление и
//    инициализацю honda в одной строке (см. видео).
    
//    Car car = {@"Honda", 2012, 132};

    
//        7. Создайте указатель на структуру honda.
//    8. Создайте цикл с пятью итерациями, в котором через УКАЗАТЕЛЬ на структуру honda
//    в каждой итерации увеличивайте количество лошадиных сил (значение переменной
//                                                             horsePower) на единицу (см. видео и презентацию).
//    После завершения цикла выведите значение honda.horsePower в консоль.
    
    Car car, *carPointer;
    carPointer = &car;
    car.name = @"Honda";
    (*carPointer).productionYear = 2012;
    carPointer->horsePower = 132;
    
    
    for (int i=0; i<5; i++) {
        (carPointer->horsePower)++;
    }

    NSLog(@"horsePower = %lu", (unsigned long)car.horsePower);
    

//    9. Создайте класс Orange (Cmd + N или File > New File), проследите, чтобы он
//    наследовал от NSObject (строка под именем класса).
//    10. В файле интерфейса класса Orange (Orange.h) объявите несколько переменных
//    экземпляра класса (не забудьте перед ними поставить @public, см. видео):
//    — NSString *color
//    — NSString *taste
//    — int radius
//    11. В теле функции main (файл main.m) создайте объект Orange с именем someOrange
//    (тут внимательно смотрите видео, все шаги там разобраны, как следует разберитесь в
//    происходящем!).
    
    Orange *someOrange = [[Orange alloc]init];
    
    //    12. Проинициализируйте все переменные объекта someOrange следующими
    //    значениями:
    //    color — Orange
    //    taste — Sweet
    //    radius — 95
    
    someOrange.color = @"Orange";
    someOrange.taste = @"Sweet";
    someOrange.radius = 95;
    

//    13. Выведите в консоль сообщение «Orange has <...> color and <...> taste», подставляя вместо пропусков значения соответствующих переменных (прямо в NSLog'е, не создавайте для них дополнительных локальных переменных).
    
    NSLog(@"Orange has %@ color and %@ taste",someOrange.color, someOrange.taste);
    
    
//    14. Создайте в main переменную и присвойте ей значение объёма апельсина (число Пи в Objeсtive-C можно получить через константу M_PI, формула расчёта есть в этих ваших интернетах). Напоминаю, радиус апельсина у вас уже есть. Выведите значение orangeVolume в консоль
    
//    И немного забегая вперёд, задание на лучшее понимание методов (помните, это такие
//    «функции» классов и их экземпляров?).
//    15. В файле интерфейса класса Orange (Orange.h) объявите метод:
//    — (void)orangeVolume;
//    Синтаксис именно такой, где писать — см. видео.
//    16. В файле реализации класса Orange реализуйте метод orangeVolume:
//    — (void)orangeVolume {
//        // Тут напишите код.
//    }
//    17. Перенесите расчёт объёма апельсина в этот метод и там же выводите его результат
//    в консоль.
//    18. В функции main вызовите метод orangeVolume объекта someOrange.
//
    
    double volume = [someOrange orangeVolume];
    NSLog(@"volume %f", volume);
    
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
