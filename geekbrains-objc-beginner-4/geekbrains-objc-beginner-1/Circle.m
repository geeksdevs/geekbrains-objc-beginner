//
//  Circle.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import "Circle.h"

@implementation Circle

- (CGFloat)square {
    return M_PI * self.width/2 * self.width/2;
}

- (CGFloat)perimeter {
    return 2 * M_PI * self.width/2;
}

- (void)description {
    NSLog(@"Площадь круга = %f, периметр = %f", [self square], [self perimeter]);
}

@end
