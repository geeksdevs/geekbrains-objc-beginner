//
//  Ellipse.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import "Ellipse.h"

@implementation Ellipse

- (CGFloat)square {
    return M_PI * self.width * self.height;
}

- (CGFloat)perimeter {
    return 4 * ((M_PI * self.width * self.height) + (self.width - self.height)) / (self.width + self.height);
}

- (void)description {
        NSLog(@"Площадь эллипса = %f, периметр = %f", [self square], [self perimeter]);
}

@end
