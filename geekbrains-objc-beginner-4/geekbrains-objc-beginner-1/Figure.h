//
//  Figure.h
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Figure : NSObject

- (instancetype)initWithHeight:(CGFloat)height andWidth:(CGFloat)width;

@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat width;

@property (strong, nonatomic) Figure *next;

- (CGFloat)square;
- (CGFloat)perimeter;

- (void)description;

@end
