//
//  Figure.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import "Figure.h"

@implementation Figure

- (instancetype)initWithHeight:(CGFloat)height andWidth:(CGFloat)width {
    if (self = [super init]) {
        _height = height;
        _width = width;
    }
    return self;
}

- (CGFloat)square {
    return 0;
}

- (CGFloat)perimeter {
    return 0;
}

- (void)description {
    
}



@end
