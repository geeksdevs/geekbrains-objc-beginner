//
//  Rectangle.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 07.02.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

- (CGFloat)square {
    return self.width * self.height;
}

- (CGFloat)perimeter {
    return (self.width + self.height) * 2;
}

- (void)description {
    NSLog(@"Площадь эллипса = %f, периметр = %f", [self square], [self perimeter]);
}

@end
