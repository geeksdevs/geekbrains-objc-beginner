//
//  main.m
//  geekbrains-objc-beginner-1
//
//  Created by Mikhail on 26.01.17.
//  Copyright © 2017 Mikhail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "Figure.h"
#import "Circle.h"
#import "Rectangle.h"
#import "Ellipse.h"

Figure* createChain() {
//    Figure *figureA = [[Figure alloc] init];
//    Figure *figureB = [[Figure alloc] init];
//    figureA.next = figureB;
//    Figure *figureC = [[Figure alloc] init];
//    figureB.next = figureC;
//    Figure *figureD = [[Figure alloc] init];
//    figureC.next = figureD;
//    Figure *figureE = [[Figure alloc] init];
//    figureD.next = figureE;
//    

    
    
    Figure *lastFigure = nil;
    for (int i = 0; i < 10; i++) {
        Figure *newFigure = [[Figure alloc] init];
        if (lastFigure) {
            lastFigure.next = newFigure;
        }
        lastFigure = newFigure;
    }
    
    return lastFigure;
}


int main(int argc, char * argv[]) {

    Circle *circ = [[Circle alloc]initWithHeight:100 andWidth:100];
    
    Rectangle *rect = [[Rectangle alloc]initWithHeight:100 andWidth:200];
    
    Ellipse *ell = [[Ellipse alloc]initWithHeight:100 andWidth:200];
    
    [circ description];
    [rect description];
    [ell description];
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
